'use strict';

const koa = require('koa');
const router = require('koa-router')();
const parser = require('koa-bodyparser');
const serve = require('koa-static');
const events = require('events');

const dispatcher = new events.EventEmitter();
const app = koa();

router.post('/send', parser(), function *() {
    dispatcher.emit('message', this.request.body);
    this.response.set('Content-Type', 'text/plain;charset=utf-8');
    this.body = 'ok';
});

router.get('/subscribe', function *() {
    this.response.set('Content-Type', 'application/json;charset=utf-8');
    this.response.set('Cache-Control', 'no-cache, must-revalidate');

    yield cb => {
        dispatcher.once('message', message => {
            this.body = message;
            cb();
        });
    };
});

app.use(router.routes()).use(router.allowedMethods()).use(serve('../../client'));

app.listen(process.env.PORT || 8080, process.env.IP || '0.0.0.0');
