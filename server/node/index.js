'use strict';

const http = require('http');
const url = require('url');
const serve = require('node-static');
const events = require('events');

const dispatcher = new events.EventEmitter();
const statics = new serve.Server('../../client');

const server = http.createServer((req, res) => {
    let parsed = url.parse(req.url, true);

    if (parsed.pathname == '/subscribe') {
        res.setHeader('Content-Type', 'text/plain;charset=utf-8');
        res.setHeader('Cache-Control', 'no-cache, must-revalidate');

        dispatcher.once('message', message => {
            res.end(message);
        });

        return;
    }

    if (parsed.pathname == '/send' && req.method == 'POST') {
        req.setEncoding('utf8');
        res.setHeader('Content-Type', 'application/json;charset=utf-8');
        let message = '';
        req.on('data', chunk => message += chunk).on('end', () => {
            dispatcher.emit('message', message);
            res.end('ok');
        });

        return;
    }

    statics.serve(req, res);
});

server.listen(process.env.PORT || 8080, process.env.IP || '0.0.0.0');
