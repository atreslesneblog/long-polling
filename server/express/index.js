'use strict';

const express = require('express');
const parser = require('body-parser');
const events = require('events');

const dispatcher = new events.EventEmitter();
const app = express();

app.post('/send', parser.json(), (req, res) => {
    dispatcher.emit('message', JSON.stringify(req.body));
    res.set('Content-Type', 'text/plain;charset=utf-8');
    res.end('ok');
});

app.get('/subscribe', (req, res) => {
    res.set('Content-Type', 'application/json;charset=utf-8');
    res.set('Cache-Control', 'no-cache, must-revalidate');

    dispatcher.once('message', message => res.end(message));
});

app.use(express.static('../../client'));

app.listen(process.env.PORT || 8080, process.env.IP || '0.0.0.0');
