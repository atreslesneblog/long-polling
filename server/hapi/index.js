'use strict';

const Hapi = require('hapi');
const inert = require('inert');
const events = require('events');
const path = require('path');

const dispatcher = new events.EventEmitter();
const server = new Hapi.Server({
    connections: {
        routes: {
            files: {
                relativeTo: path.join(__dirname, '../../client')
            }
        }
    }
});
server.connection({ port: process.env.PORT || 8080 });
server.register(inert, () => {});

server.route({
    method: 'POST',
    path: '/send',
    handler: (req, res) => {
        dispatcher.emit('message', req.payload);
        res('ok').type('text/plain');
    }
});

server.route({
    method: 'GET',
    path: '/subscribe',
    handler: (req, res) => {
        dispatcher.once('message', message => {
            res(message).type('text/plain').header('Cache-Control', 'no-cache, must-revalidate');
        });
    }
});

server.route({
    method: 'GET',
    path: '/{param*}',
    handler: {
        directory: {
            path: '.',
            redirectToSlash: true,
            index: true
        }
    }
});

server.start();
